import { ExampleReactiveFormComponent } from './example-reactive-form/example-reactive-form.component';
import { MyThirdComponent } from './my-third/my-third.component';
import { MySecondComponent } from './my-second/my-second.component';
import { MyFirstComponent } from './my-first/my-first.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: MyFirstComponent },
  { path: 'second', component: MySecondComponent },
  { path: 'third', component: MyThirdComponent },
  { path: 'reactive-forms', component: ExampleReactiveFormComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

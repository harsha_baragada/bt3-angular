import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'sam-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  title = 'My logo';

  constructor(private readonly router: Router) {}

  ngOnInit(): void {}

  goToThirdComponent() {
    this.router.navigate(['/third']);
  }
}

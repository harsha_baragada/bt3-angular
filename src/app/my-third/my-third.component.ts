import { FirstService } from './../service/first.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sam-my-third',
  templateUrl: './my-third.component.html',
  styleUrls: ['./my-third.component.css'],
})
export class MyThirdComponent implements OnInit {
  name: string;
  posts: any = [];
  errorMessage: string;
  isError: boolean;
  constructor(private readonly firstService: FirstService) {}

  ngOnInit(): void {
    this.firstService.getPosts().subscribe((respose) => {
      console.log(respose);
      this.posts = respose;
      this.posts = Array.of(this.posts);
    });
  }

  submitInput() {
    this.resetError();
    if (!this.name) {
      this.isError = true;
      this.errorMessage = 'This field should not be empty';
    } else {
      alert(this.name);
    }
  }
  resetError() {
    this.isError = false;
  }
}
